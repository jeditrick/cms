-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: uwc7
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `address` int(11) NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Nike','asddasd',0,'','2015-04-17 22:20:40','2015-04-18 09:10:06'),(2,'Apple','',0,'','2015-04-17 22:26:25','2015-04-18 08:58:58'),(3,'TeaTeam','',0,'','2015-04-17 22:41:21','2015-04-18 08:59:11'),(5,'New Balance','',0,'','2015-04-18 11:17:32','2015-04-18 11:17:32');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `short_description` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `brand` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

LOCK TABLES `goods` WRITE;
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (1,'Товари','',0,'','','',2,'2015-04-19 07:40:21','2015-04-19 07:40:21',0),(2,'Засоби гігієни','',1,'','','',2,'2015-04-19 07:41:39','2015-04-19 07:41:39',0),(3,'Взуття','',1,'','','',2,'2015-04-19 07:41:48','2015-04-19 07:41:48',0),(4,'Сумки','',1,'','','',2,'2015-04-19 07:42:05','2015-04-19 07:42:05',0),(5,'Одяг','',1,'','','',2,'2015-04-19 07:42:25','2015-04-19 07:42:25',0),(6,'Wear item 1','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 07:52:11','2015-04-19 07:52:11',1),(7,'Shoes item 1','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','2015_04_19_IMG_20131222_155646.jpg',1,'2015-04-19 07:52:11','2015-04-19 14:03:24',1),(8,'Bags item 1','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:11','2015-04-19 07:52:11',3),(9,'Wear item 2','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 07:52:22','2015-04-19 07:52:22',1),(10,'Shoes item 2','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:22','2015-04-19 07:52:22',1),(11,'Bags item 2','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:22','2015-04-19 07:52:22',3),(12,'Wear item 3','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 07:52:27','2015-04-19 07:52:27',1),(13,'Shoes item 3','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:27','2015-04-19 07:52:27',1),(14,'Bags item 3','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:27','2015-04-19 07:52:27',3),(15,'Wear item 4','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 07:52:32','2015-04-19 07:52:32',1),(16,'Shoes item 4','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:32','2015-04-19 07:52:32',1),(17,'Bags item 4','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:32','2015-04-19 07:52:32',3),(18,'Wear item 5','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 07:52:37','2015-04-19 07:52:37',1),(19,'Shoes item 5','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:38','2015-04-19 07:52:38',1),(20,'Bags item 5','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:38','2015-04-19 07:52:38',3),(21,'Wear item 6','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 07:52:42','2015-04-19 07:52:42',1),(22,'Shoes item 6','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:42','2015-04-19 07:52:42',1),(23,'Bags item 6','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 07:52:42','2015-04-19 07:52:42',3),(24,'Статті','',0,'','','',2,'2015-04-19 07:53:28','2015-04-19 07:53:28',0),(25,'Блог','',24,'','','',2,'2015-04-19 07:54:25','2015-04-19 07:54:25',0),(26,'Новини','',24,'','','',2,'2015-04-19 07:54:35','2015-04-19 07:54:35',0),(27,'Події','',24,'','','',2,'2015-04-19 07:54:43','2015-04-19 07:54:43',0),(28,'Сайт','',24,'','','',2,'2015-04-19 07:54:56','2015-04-19 07:54:56',0),(29,'Про нас','',28,'фівфівфівіф','вфівфівфів','',2,'2015-04-19 07:55:10','2015-04-19 13:37:28',0),(30,'Blog item 1','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:06','2015-04-19 08:05:06',0),(31,'Event item 1','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:06','2015-04-19 08:05:06',0),(32,'News item 1','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:06','2015-04-19 08:05:06',0),(33,'Blog item 2','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:17','2015-04-19 08:05:17',0),(34,'Event item 2','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:18','2015-04-19 08:05:18',0),(35,'News item 2','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:18','2015-04-19 08:05:18',0),(36,'Blog item 3','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:22','2015-04-19 08:05:22',0),(37,'Event item 3','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:23','2015-04-19 08:05:23',0),(38,'News item 3','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:23','2015-04-19 08:05:23',0),(39,'Blog item 4','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:26','2015-04-19 08:05:26',0),(40,'Event item 4','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:26','2015-04-19 08:05:26',0),(41,'News item 4','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:26','2015-04-19 08:05:26',0),(42,'Blog item 5','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:30','2015-04-19 08:05:30',0),(43,'Event item 5','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:30','2015-04-19 08:05:30',0),(44,'News item 5','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:05:30','2015-04-19 08:05:30',0),(45,'Wear item 8','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 08:08:54','2015-04-19 08:08:54',2),(46,'Shoes item 8','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:54','2015-04-19 08:08:54',5),(47,'Bags item 8','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:54','2015-04-19 08:08:54',3),(48,'Blog item 8','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:54','2015-04-19 08:08:54',0),(49,'Event item 8','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:54','2015-04-19 08:08:54',0),(50,'News item 8','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:54','2015-04-19 08:08:54',0),(51,'Wear item 8','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 08:08:55','2015-04-19 08:08:55',2),(52,'Shoes item 8','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:55','2015-04-19 08:08:55',5),(53,'Bags item 8','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:55','2015-04-19 08:08:55',3),(54,'Blog item 8','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:55','2015-04-19 08:08:55',0),(55,'Event item 8','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:56','2015-04-19 08:08:56',0),(56,'News item 8','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:08:56','2015-04-19 08:08:56',0),(57,'Wear item 9','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 08:09:03','2015-04-19 08:09:03',2),(58,'Shoes item 9','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:03','2015-04-19 08:09:03',5),(59,'Bags item 9','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:03','2015-04-19 08:09:03',3),(60,'Blog item 9','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:03','2015-04-19 08:09:03',0),(61,'Event item 9','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:03','2015-04-19 08:09:03',0),(62,'News item 9','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:03','2015-04-19 08:09:03',0),(63,'Wear item 10','',5,'Nice wear very cool','very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!','',1,'2015-04-19 08:09:07','2015-04-19 08:09:07',2),(64,'Shoes item 10','',3,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:07','2015-04-19 08:09:07',5),(65,'Bags item 10','',4,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:07','2015-04-19 08:09:07',3),(66,'Blog item 10','Kolya',25,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:08','2015-04-19 08:09:08',0),(67,'Event item 10','Kolya',27,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:08','2015-04-19 08:09:08',0),(68,'News item 10','Kolya',26,'short short short short short short short short short short short short short short short short','long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ','',1,'2015-04-19 08:09:08','2015-04-19 08:09:08',0),(69,'Виробникам','',28,'','','',2,'2015-04-19 13:37:56','2015-04-19 13:37:56',0),(70,'Користувачам','',28,'','','',2,'2015-04-19 13:38:08','2015-04-19 13:38:08',0),(71,'про нас ітем','',29,'','','',1,'2015-04-19 13:51:33','2015-04-19 13:51:33',0),(72,'Виробникам ітем','',69,'','','',1,'2015-04-19 13:51:45','2015-04-19 13:51:45',0),(73,'Користувачам ітем','',70,'','','',1,'2015-04-19 13:51:56','2015-04-19 13:51:56',0);
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_04_15_182022_create_goods_table',1),('2015_04_18_005436_create_brands_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'admin','admin@admin.ua','$2y$10$xI5BtLwG97jnjF6Csk10jeqqSlgoI6lv4a03eYYs1Hfq3SV6SCU.K',NULL,'2015-04-18 16:00:30','2015-04-18 16:00:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-19 20:12:09

<!DOCTYPE html>
<!--[if IE 8]>
<html class = "ie8">
<![endif]-->
<!--[if !IE]>-->
<html>
<!--<![endif]-->
<head lang = "en">
    @include('includes.site')
</head>
<body>
@include('page.header')
<section class = "blog">
    <div class = "container">
        <h1 class = "blog__title">{{get_obj($data['id'])->name}}</h1>

        <div class = "blog__posts">
            @foreach(array_chunk($data['articles'],2) as $row)
                <div class = "blog__posts__column">
                    @foreach($row as $article)
                        <article class = "blog__post">
                            <img class = "blog__post__cover" src = "{{ route('get_photo',[$article['id'],'item']) }}" alt = ""/>
                            <h2 class = "blog__post__title">{{$article['name']}}</h2>
                            <div class = "blog__post__text">
                                <p>{!!$article['short_description']!!}...<a class = "blog__post__permalink" href = "{{url('article/'.$article['id'])}}">далі &gt;</a></p>
                            </div>
                        </article>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
</section>
@include('page.search')
@include('page.popular_brands')
@include('page.footer')
<script src = "{{asset('/js/all.js')}}"></script>
</body>
</html>
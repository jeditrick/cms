@extends('app')

@section('content')
    <h2>{{$data['headline']}}</h2>
    {!! Form::open(['url' => 'home/brand/submit','files' => true]) !!}
    <div class = "form-group">
        {!! Form::label('name', 'Name:') !!}
        @if( isset($data['item']['name']))
        {!! Form::text('name', $data['item']['name'],['class' => 'form-control']) !!}
        @else
        {!! Form::text('name', null,['class' => 'form-control']) !!}
        @endif
    </div>

    <div class = "form-group">
        {!! Form::label('description', 'Description:') !!}
        @if( isset($data['item']['description']))
        {!! Form::textarea('description', $data['item']['description'],['class' => 'form-control', 'id' => 'editor']) !!}
        @else

        {!! Form::textarea('description', null,['class' => 'form-control', 'id' => 'editor']) !!}
        @endif
    </div>
    @if(isset($data['item']['id']))
        <input type = "hidden" value = "{{$data['item']['id']}}" name = "id"/>
    @endif
    <div class = "form-group">
        @if($has_photo )
            <img src=" {{ route('get_photo',[$data['item']['id'],'brands']) }}"  class="img-thumbnail">
            {!! Html::link(route('delete_photo',[$data['item']['id'],'brands']), 'Delete Photo', ['class' => 'btn btn-warning']) !!}
        @endif
            {!! Form::file('image') !!}

    </div>
    <div class = "form-group">
        {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
    </div>


    {!! Form::close() !!}

    @if($errors->any())
        <ul class = "alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </ul>
    @endif

@endsection
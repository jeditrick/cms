@extends('app')

@section('content')
    <h2>List of Brands</h2>
    @if(count($brands))
        <table class = "table table-hover">
            <tr>
                <td><b>ID</b></td>
                <td><b>Name</b></td>
                <td><b>Created</b></td>
                <td><b>Updated</b></td>
                <td><b>Operations</b></td>
            </tr>
            @foreach($brands as $brand)
                <tr>
                    <td>{{ $brand['id'] }}</td>
                    <td>{{ $brand['name'] }}</td>
                    <td>{{ $brand['created_at'] }}</td>
                    <td>{{ $brand['updated_at'] }}</td>
                    <td>
                        <a href = "{{ url('home/brand/'.$brand['id'].'/edit')  }}">Edit</a>,
                        <a class = "text-danger" href = "{{ url('home/brand/'.$brand['id'].'/delete')  }}">Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <div class = "bg-warning">There is no items! add them :)</div>
    @endif
    <hr/>
    <a href = "{{ url('home/brand/add') }}" class = "btn btn-primary btn-lg active" role = "button">Add item</a>
@endsection
@extends('app')

@section('content')
    <h2>{{$data['headline']}}</h2>
    <hr/>
    <ul class = "goods__catalogue__breadcrumbs">
        <li style="display: inline-block;"><a href = "{{url('home/goods')}}">Corner</a></li>
        @foreach($breadcrumbs as $crumb)
            <li style="display: inline-block;">> <a href = "{{url(('home/item/'.$crumb->id.'/'.(($crumb->type===1)?'edit':'open')).'')}}">{{$crumb->name}} </a></li>
        @endforeach
    </ul>
    <hr/>
    {!! Form::open(['url' => 'home/submit_item','files' => true]) !!}
    <div class = "form-group">
        {!! Form::label('name', 'Name:') !!}
        @if( isset($data['item']['name']))
        {!! Form::text('name', $data['item']['name'],['class' => 'form-control']) !!}
        @else
        {!! Form::text('name', null,['class' => 'form-control']) !!}
        @endif
    </div>
    <div class = "form-group">
        {!! Form::label('name', 'Author:') !!}
        @if( isset($data['item']['author']))
        {!! Form::text('author', $data['item']['author'],['class' => 'form-control']) !!}
        @else
        {!! Form::text('author', null,['class' => 'form-control']) !!}
        @endif
    </div>
    <div class = "form-group">
        <label for = "type">Brands</label>
        <select name = "brand" class = "form-control" id = "type">
            <option value = "0"></option>
            @foreach($data['brands'] as $brand)
                @if(isset($data['item']['brand'])))
                <option {{($brand->id === $data['item']['brand'])?'selected':''}}
                        value = "{{$brand->id}}">{{$brand->name}}</option>
                @else
                    <option value = "{{$brand->id}}">{{$brand->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class = "form-group">
        {!! Form::label('description', 'Description:') !!}
        @if( isset($data['item']['description']))
        {!! Form::textarea('description', $data['item']['description'],['class' => 'form-control', 'id' => 'editor'])
        !!}
        @else

        {!! Form::textarea('description', null,['class' => 'form-control', 'id' => 'editor']) !!}
        @endif
    </div>
    <div class = "form-group">
        {!! Form::label('short_description', 'Short description:') !!}
        @if( isset($data['item']['short_description']))
        {!! Form::textarea('short_description', $data['item']['short_description'],['class' => 'form-control', 'id' =>
        'short_editor']) !!}
        @else

        {!! Form::textarea('short_description', null,['class' => 'form-control', 'id' => 'editor']) !!}
        @endif
    </div>
    <div class = "form-group">
        <label for = "type">Type</label>
        <select name = "type" class = "form-control" id = "type">
            <option value = "1" {{ (isset($data['item'])&&$data['item']['type'] == 1)?'selected':'' }}>Item</option>
            <option value = "2" {{ (isset($data['item'])&&$data['item']['type'] == 2)?'selected':'' }}>Folder</option>
        </select>
    </div>


    @if(isset($data['item']['id']))
        <input type = "hidden" value = "{{$data['item']['id']}}" name = "id"/>
    @endif
    <div class = "form-group">
        @if($has_photo )
            <img src = " {{ route('get_photo',[$data['item']['id'],'item']) }}" class = "img-thumbnail">
            {!! Html::link(route('delete_photo',[$data['item']['id'],'item']), 'Delete Photo', ['class' => 'btn
            btn-warning'])
            !!}
        @endif
            {!! Form::file('image') !!}

    </div>
    <div class = "form-group">
        {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
    </div>

    @if(isset($folder_id))
        <input type = "hidden" value = "{{ $folder_id }}" name = "parent_id"/>
    @endif

    {!! Form::close() !!}

    @if($errors->any())
        <ul class = "alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </ul>
    @endif

@endsection
@extends('app')

@section('content')
    <h2>List of items</h2>
    <hr/>
    <ul class = "goods__catalogue__breadcrumbs">
        <li style="display: inline-block;"><a href = "{{url('home/goods')}}">Corner</a></li>
        @foreach($breadcrumbs as $crumb)
            <li style="display: inline-block;">> <a href = "{{url('home/item/'.$crumb->id.'/open')}}">{{$crumb->name}} </a></li>
        @endforeach
    </ul>
    <hr/>

    @if(count($goods))
        <table class = "table table-hover">
            <tr>
                <td><b>ID</b></td>
                <td><b>Type</b></td>
                <td><b>Name</b></td>
                <td><b>Created</b></td>
                <td><b>Updated</b></td>
                <td><b>Operations</b></td>
            </tr>
            @foreach($goods as $good)
                <tr>
                    <td>{{ $good['id'] }}</td>
                    @if($good['type']==1)
                        <td><span class = "glyphicon glyphicon-file" aria-hidden = "true"></span></td>
                    @elseif($good['type']==2)
                        <td><span class = "glyphicon glyphicon-folder-open" aria-hidden = "true"></span></td>
                    @endif
                    <td>{{ $good['name'] }}</td>
                    <td>{{ $good['created_at'] }}</td>
                    <td>{{ $good['updated_at'] }}</td>
                    <td>
                        <a href = "{{ url('home/item/'.$good['id'].'/edit')  }}">Edit</a>,
                        <a class = "text-danger" href = "{{ url('home/item/'.$good['id'].'/delete')  }}">Delete</a>
                        @if($good['type'] == 2)
                            <a class = "text-success" href = "{{ url('home/item/'.$good['id'].'/open') }}">Open</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <div class = "bg-warning">There is no items! add them :)</div>
    @endif

    <hr/>
    @if($folder_id !== null)
        <a href = "{{ url('home/item/'.$folder_id.'/add') }}" class = "btn btn-primary btn-lg active" role = "button">Add item</a>
    @else
        <a href = "{{ url('home/item/0/add') }}" class = "btn btn-primary btn-lg active" role = "button">Add item</a>
    @endif
    {{--<a href = "{{ url('home/back') }}" class = "btn btn-primary btn-lg active" role = "button">Back</a>--}}

@endsection
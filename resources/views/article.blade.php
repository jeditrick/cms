<!DOCTYPE html>
<!--[if IE 8]>
<html class = "ie8">
<![endif]-->
<!--[if !IE]>-->
<html>
<!--<![endif]-->
<head lang = "en">
    @include('includes.site')
</head>
<body>
@include('page.header')
<section class = "blog">
    <div class = "container">
        <h1 class = "blog__title">{{$data['article']->name}}</h1>
    </div>
    <div class="content">
        {!!$data['article']->description!!}
    </div>
</section>
@include('page.search')
@include('page.footer')
<script src = "{{asset('/js/all.js')}}"></script>
</body>
</html>
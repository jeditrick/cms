<section class = "categories">
    <div class = "container">
        <div class = "categories__description">
            <p>Наразі купувати українські продукти та речі не тільки вигідно, а є вже правилом гарного тону.<br/>Українські
               виробники виробляють майже все: від колготок до танків, від іграшок до спортивного інвентарю.</p>

            <p>На сайті зібрано повний каталог українських виробників.</p>
        </div>
        @foreach($data['goods_tree'] as $id =>$cat)
            @if($i++%4===0)
                <div class = "categories__row">
                    <ul class = "categories__tabs-btns">
                        @endif
                        <li class = "categories__tabs-btn categories__tabs-btn__hygiene" data-tab = "hygiene">
                            <a href = "{{url('items/cat/'.get_obj($id)->id)}}">
                                <span>{{get_obj($id)->name}}</span>
                            </a>
                        </li>
                        @if($i%4===0)
                    </ul>
                </div>
            @endif
        @endforeach
    </div>
</section>
<section class = "manufacturers">
    <h2 class = "manufacturers__title">НАйпопулярніші українські виробники</h2>
    <div class = "manufacturers__logos">
        <div class = "container">
            <ul class = "manufacturers__logos__list">
                @foreach($data['popular_brands'] as $brand)
                    <li class = "emttonmodern"><a href = "{{url('brands/'.get_brand($brand->id,'id').'/search')}}">{{ $brand->name }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</section>
<header class = "site-header">
    <div class = "container">
        <button class = "mobile-menu-btn">
            <span></span>
        </button>

        <a class = "site-logo" href = "{{url('/')}}"><span class = "site-logo__ua">ua</span><span
                    class = "site-logo__cat">каталог</span></a>

        <div class = "catalogue-menu">
            <a class = "catalogue-menu__btn" href = "#">
                <i class = "icon icon--catalogue-menu"></i>
            </a>
            <ul class = "catalogue-menu__list">
                @foreach($data['goods_tree'] as $id =>$cat)
                    <li class = "catalogue-menu__list__item">
                        <a href = "{{url('items/cat/'.get_obj($id)->id)}}">{{get_obj($id)->name}}</a>
                        @if(has_children($cat))
                            <ul class = "catalogue-menu__list__sub-one">
                                @foreach($cat as $sub_id => $sub_cat)
                                    <li class = "catalogue-menu__list__sub-one__item"><a
                                                href = "{{url('items/cat/'.get_obj($sub_id)->id)}}">{{get_obj($sub_id)->name}}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach

            </ul>
        </div>
        <nav class = "primary-nav">
            <ul class = "primary-nav__list">
                <li class = "primary-nav__list__item">
                    <a href = "{{url('article/29')}}">Про нас</a>
                </li>
                <li class = "primary-nav__list__item">
                    <a href = "{{url('events')}}">Події</a>
                </li>
                <li class = "primary-nav__list__item">
                    <a href = "{{url('news')}}">Новини</a>
                </li>
                <li class = "primary-nav__list__item">
                    <a href = "{{url('blog')}}">Блог</a>
                </li>
            </ul>
        </nav>

    </div>
</header>
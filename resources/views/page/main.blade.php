<section class="catalogue-search">
        <div class="container">
            <div class="catalogue-search__slider">
                <div class="catalogue-search__slider__slide">
                    <img src="img/slider.jpg" alt=""/>
                </div>
                <div class="catalogue-search__slider__controls">
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="catalogue-search__search-block">
                <h1 class="catalogue-search__search-block__title">Повний каталог українських виробників</h1>

                <form class="catalogue-search__search-block__form" action="action.php">
                    <input type="text" placeholder="Назва товару"/>
                    <button type="submit">
                        <i class="icon icon--search-btn"></i>
                    </button>
                </form>
            </div>
        </div>
    </section>
    <!-- catalogue search section - конец -->
    <!-- catalogue section - начало -->
    <section class="categories">
        <div class="container">
            <div class="categories__description">
                <p>Наразі купувати українські продукти та речі не тільки вигідно, а є вже правилом гарного тону.<br/>Українські виробники виробляють майже все: від колготок до танків, від іграшок до спортивного інвентарю.</p>
                <p>На сайті зібрано повний каталог українських виробників.</p>
            </div>
            <div class="categories__row">
                <ul class="categories__tabs-btns">
                    <li class="categories__tabs-btn categories__tabs-btn__hygiene" data-tab="hygiene">
                        <a href="#">
                            <i class="icon icon--hygiene"></i>
                            <span>Засоби гігієни</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn" data-tab="clothes">
                        <a href="#">
                            <i class="icon icon--clothes"></i>
                            <span>Одяг</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--shoes"></i>
                            <span>Взуття</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--bags"></i>
                            <span>Валізи та сумки</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="categories__row">
                <ul class="categories__tabs-btns">
                    <li class="categories__tabs-btn categories__tabs-btn__hygiene">
                        <a href="#">
                            <i class="icon icon--dishes"></i>
                            <span>Посуд</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--makeup"></i>
                            <span class="two">Декоративна косметика</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--for-kids"></i>
                            <span class="two">Товари для дітей</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--food"></i>
                            <span class="two">Продукти харчування</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="categories__row">
                <ul class="categories__tabs-btns">
                    <li class="categories__tabs-btn categories__tabs-btn__hygiene">
                        <a href="#">
                            <i class="icon icon--drinks"></i>
                            <span class="two">Алкогольні напої</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--figgery"></i>
                            <span>Прикраси</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--jewelry"></i>
                            <span class="two">Ювелірні вироби</span>
                        </a>
                    </li>
                    <li class="categories__tabs-btn">
                        <a href="#">
                            <i class="icon icon--other"></i>
                            <span>Різне</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- catalogue section - конец -->
    <!-- goods - начало -->
    <section class="goods">
        <div class="goods__header">
            <div class="container">
                <h2 class="goods__title">Жіночий одяг</h2>
                <div class="goods__sort-options">
                    <div class="goods__sort-options__wrapper">
                        <span>Сортувати за:</span>
                        <label class="goods__sort-options__option">
                            <select name="sort" id="date">
                                <option>Датою оновлення</option>
                                <option>Датою оновлення 2</option>
                                <option>Датою оновлення 3</option>
                            </select>
                        </label>
                    </div>
                    <div class="goods__sort-options__wrapper">
                        <span>Обрати регіон:</span>
                        <label class="goods__sort-options__option">
                            <select name="sort" id="sort">
                                <option>Не обрано</option>
                                <option>Не обрано 2</option>
                                <option>Не обрано 3</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="goods__content">
            <div class="container">
                <div class="goods__settings">
                    <div class="dropbox dropped">
                        <h3><a href="#">Тип<i class="icon icon--dropbox"></i></a></h3>
                        <ul>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_1" id="cat_1"/>
                                    <label for="cat_1">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Костюми</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_2" id="cat_2"/>
                                    <label for="cat_2">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Піжаки</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_3" id="cat_3"/>
                                    <label for="cat_3">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Светри і гольфи</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_4" id="cat_4"/>
                                    <label for="cat_4">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Плаття</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_5" id="cat_5"/>
                                    <label for="cat_5">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Спідниці</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_6" id="cat_6"/>
                                    <label for="cat_6">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Брюки</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_7" id="cat_7"/>
                                    <label for="cat_7">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Блузи</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_8" id="cat_8"/>
                                    <label for="cat_8">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Білизна</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input type="checkbox" name="cat_9" id="cat_9"/>
                                    <label for="cat_9">
                                        <i class="icon icon--chekbox"></i>
                                        <span>Спортивний одяг</span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <h3>Ціновий діапазон</h3>
                    <div class="price-range">
                        <div class="price-range__line"></div>
                        <div class="price-range__line--fill"></div>
                        <div class="price-range__thumb--first"></div>
                        <div class="price-range__thumb--second"></div>
                        <span class="price-range__price--first">550 ₴</span>
                        <span class="price-range__price--second">7580 ₴</span>
                    </div>
                    <div class="dropbox">
                        <h3><a href="#">Бренд<i class="icon icon--dropbox"></i></a></h3>
                    </div>
                </div>
                <div class="goods__catalogue">
                    <ul class="goods__catalogue__breadcrumbs">
                        <li><a href="#">Одяг</a></li>
                        <li><a href="#">Жіночий одяг</a></li>
                    </ul>
                    <div class="goods__container">
                        <div class="goods__wrapper">
                            <article class="good__card good__card--margin">
                                <div class="good__photo">
                                    <a class="good__image" href="#">
                                        <img src="img/goods/1.jpg" alt=""/>
                                        <i class="icon icon--show-preview"></i>
                                    </a>
                                    <span class="good__photo__tag show">новий</span>
                                </div>
                                <h4 class="good__title">Платье на пуговицах</h4>
                                <span class="good__manufacturer">Grâce à vous</span>
                                <span class="good__price">1 450,00 ₴</span>
                                <button class="good__to-favourite good__btn">В обране</button>
                            </article>
                        </div>
                        <div class="goods__wrapper goods__wrapper--center">
                            <article class="good__card good__card--margin">
                                <div class="good__photo">
                                    <a class="good__image" href="#">
                                        <img src="img/goods/2.jpg" alt=""/>
                                        <i class="icon icon--show-preview"></i>
                                    </a>
                                    <span class="good__photo__tag show">новий</span>
                                </div>
                                <h4 class="good__title">Блузка шифон</h4>
                                <span class="good__manufacturer">Cat Orange</span>
                                <span class="good__price">580,00 ₴</span>
                                <button class="good__to-favourite good__btn">В обране</button>
                            </article>
                        </div>
                        <div class="goods__wrapper goods__wrapper--right">
                            <article class="good__card good__card--margin">
                                <div class="good__photo">
                                    <a class="good__image" href="#">
                                        <img src="img/goods/3.jpg" alt=""/>
                                        <i class="icon icon--show-preview"></i>
                                    </a>
                                    <span class="good__photo__tag show">новий</span>
                                </div>
                                <h4 class="good__title">Платья 886&amp;103</h4>
                                <span class="good__manufacturer">Alve</span>
                                <span class="good__price">890,00 ₴</span>
                                <button class="good__to-favourite good__btn">В обране</button>
                            </article>
                        </div>
                        <div class="goods__wrapper">
                            <article class="good__card good__card__last">
                                <div class="good__photo">
                                    <a class="good__image" href="#">
                                        <img src="img/goods/4.jpg" alt=""/>
                                        <i class="icon icon--show-preview"></i>
                                    </a>
                                    <span class="good__photo__tag show">новий</span>
                                </div>
                                <h4 class="good__title">Футболка женская PATRIOT</h4>
                                <span class="good__manufacturer">Одежда с символикой Украины</span>
                                <span class="good__price">230,00 ₴</span>
                                <button class="good__to-favourite good__btn">В обране</button>
                            </article>
                        </div>
                        <div class="goods__wrapper goods__wrapper--center">
                            <article class="good__card">
                                <div class="good__photo">
                                    <a class="good__image" href="#">
                                        <img src="img/goods/5.jpg" alt=""/>
                                        <i class="icon icon--show-preview"></i>
                                    </a>
                                    <span class="good__photo__tag">новий</span>
                                </div>
                                <h4 class="good__title">Жупан короткий з відктритими швами</h4>
                                <span class="good__manufacturer">Отаман</span>
                                <span class="good__price">6700,00 ₴</span>
                                <button class="good__to-favourite good__btn">В обране</button>
                            </article>
                        </div>
                        <div class="goods__wrapper goods__wrapper--right">
                            <article class="good__card">
                                <div class="good__photo">
                                    <a class="good__image" href="#">
                                        <img src="img/goods/6.jpg" alt=""/>
                                        <i class="icon icon--show-preview"></i>
                                    </a>
                                    <span class="good__photo__tag">новий</span>
                                </div>
                                <h4 class="good__title">Вишиванка батистова з поясом</h4>
                                <span class="good__manufacturer">Отаман</span>
                                <span class="good__price">3400,00 ₴</span>
                                <button class="good__to-favourite good__btn">В обране</button>
                            </article>
                        </div>
                    </div>
                    <ul class="goods__pagination">
                        <li class="first-page"><a href="#"></a></li>
                        <li class="prev-page"><a href="#"></a></li>
                        <li class="page"><a href="#">1</a></li>
                        <li class="page"><a href="#">2</a></li>
                        <li class="page"><a href="#">3</a></li>
                        <li class="page"><a href="#">4</a></li>
                        <li class="page"><a href="#">5</a></li>
                        <li class="page"><a href="#">6</a></li>
                        <li class="page"><a href="#">7</a></li>
                        <li class="page">...</li>
                        <li class="page"><a href="#">28</a></li>
                        <li class="next-page"><a href="#"></a></li>
                        <li class="last-page"><a href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- goods - конец -->
    <!-- testimonials - начало -->
    <section class="testimonials">
        <div class="container">
            <div class="testimonials__slider">
                <div class="testimonials__slider__slide">
                    <div class="photo">
                        <img src="img/testimonial-photo.jpg" alt=""/>
                    </div>
                    <div class="text">
                        <h3 class="title">У пошуках MADE IN UKRAINE</h3>
                        <h4 class="name">Юлія Савостіна</h4>
                        <p>У лютому 2013 року пообіцяла, що буде рік купувати тільки товари українського виробництва і розповідати про всі знахідки читачам <b>свого блогу</b>. Пообіцяла і зробила.</p>
                        <p>Продовжує агітувати за все якісне і модне українське тепер не тільки в блозі, але і в якості керівника окремого напрямку <i>Made in Ukraine в Ekonomika Communication Hub</i>.</p>
                        <a class="read" href="#">Читати детальніше</a>
                    </div>
                </div>
                <a class="testimonials__slider__button testimonials__slider__button--prev" href="#"></a>
                <a class="testimonials__slider__button testimonials__slider__button--next" href="#"></a>
                <div class="testimonials__slider__controls">
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonials - конец -->
    <!-- manufacturers - начало -->
    <section class="manufacturers">
        <h2 class="manufacturers__title">НАйпопулярніші українські виробники</h2>
        <div class="manufacturers__logos">
            <div class="container">
                <ul class="manufacturers__logos__list">
                    <li class="emttonmodern"><a href="#"><i></i></a></li>
                    <li class="nenka"><a href="#"><i></i></a></li>
                    <li class="raslov"><a href="#"><i></i></a></li>
                    <li class="sem"><a href="#"><i></i></a></li>
                    <li class="tago"><a href="#"><i></i></a></li>
                    <li class="koroli"><a href="#"><i></i></a></li>
                </ul>
            </div>
        </div>
    </section>
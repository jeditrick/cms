<footer class = "site-footer">
    <div class = "container">

        @foreach($data['footer_info'] as $id=>$el)
            <div class = "site-footer__cell site-footer__cell--1">
                <div class = "site-footer__about-us">

                    <h3>{{get_obj((int)$id)->name}}</h3>
                    <ul>
                        @foreach($el as $el2)
                            <li><a href = "#">{{$el2->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
</footer>
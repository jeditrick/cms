<section class = "good-block">
    <div class = "container">
        <ul class = "goods__catalogue__breadcrumbs">
            @foreach($data['breadcrumbs'] as  $crumb)
                <li>
                    <a href = "{{url( ($crumb->type === 1)?'goods/'.$crumb->id:'items/cat/'.$crumb->id )}}">{{$crumb->name}}</a>
                </li>
            @endforeach
        </ul>
        <div class = "good-block__wrapper">
            <div class = "good-block__prev">
                <div class = "good-block__prev__main">
                    <img src = "{{ route('get_photo',[$data['goods']->id,'item']) }}" alt = ""/>
                    <div class = "good-block__prev__manuf">
                        {{$data['goods']->short_description}}
                    </div>
                </div>

            </div>
            <div class = "good-block__info">
                <h2 class = "good-block__title">{{$data['goods']->name}}</h2>

                <p class = "good-block__manufacturer">
                    <b>Виробник:</b><span>{{get_brand($data['goods']->brand,'name')}}</span></p>

                <p class = "good-block__category"><b>Категорія:</b>
                <ul class = "goods__catalogue__breadcrumbs">
                    @foreach(array_slice($data['breadcrumbs'],1,-1) as  $crumb)
                        <li>
                            <a href = "{{url( ($crumb->type === 1)?'goods/'.$crumb->id:'items/cat/'.$crumb->id )}}"> {{$crumb->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <h3 class = "good-block__description-title">Опис:</h3>

                <p class = "good-block__description">
                    {{$data['goods']->description}}
                </p>

            </div>

        </div>
        <div class = "good-block__other-goods">
            <h3 class = "good-block__other-goods__title">Інші товари виробника {{get_brand($data['goods']->brand,'name')}}</h3>
            <ul class = "good-block__other-goods__list">
                @foreach($data['alike'] as $el)
                    <li class = "good-block__other-goods__list__item good-block__other-goods__list__item--1">
                        <a href = "{{url('goods/'.$el->id)}}"><img src = "{{ route('get_photo',[$el->id,'item']) }}" alt = ""/>{{$el->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>
<section class = "goods">
    <div class = "goods__header">
        <div class = "container">
            <h2 class = "goods__title">{{$data['cat_active']->name}}</h2>
        </div>
    </div>
    <div class = "goods__content">
        <div class = "container">
            <div class = "goods__settings">
                <div class = "dropbox dropped">
                    <h3><a href = "#">Тип<i class = "icon"></i></a></h3>
                    <ul>
                        @foreach($data['types'] as $type)
                            <li>
                                <div class = "checkbox">
                                    <input type = "checkbox" name = "cat_1"
                                           id = "cat_1" {{($type->id==$data['id']?'checked':'')}}/>
                                    <label for = "cat_1">
                                        <i class = "icon icon--chekbox"></i>
                                        <a href = "{{url('items/cat/'.get_obj($type->id)->id)}}">{{get_obj($type->id)->name}}</a>
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class = "dropbox">
                    <h3><a href = "#">Бренд<i class = "icon icon--dropbox"></i></a></h3>
                    <ul>
                        @foreach($data['brands'] as $brand)
                            <li>
                                <div class = "checkbox">
                                    <input type = "checkbox" name = "cat_1"
                                           id = "cat_1" {{ ((int)$data['brand_id'] === $brand->id)?'checked':'' }}/>

                                    <label for = "cat_1">
                                        <i class = "icon icon--chekbox"></i>
                                        <a href = "{{url('brands/'.get_brand($brand->id,'id').'/search')}}">{{get_brand($brand->id,'name')}}</a>
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class = "goods__catalogue">
                <ul class = "goods__catalogue__breadcrumbs">
                    @foreach($data['breadcrumbs'] as $crumb)
                        <li><a href = "{{url('items/cat/'.$crumb->id)}}">{{$crumb->name}}</a></li>
                    @endforeach
                </ul>

                <div class = "goods__container">
                    @foreach($data['cat_goods'] as $item)

                        <div class = "goods__wrapper">
                            <article class = "good__card good__card--margin">
                                <div class = "good__photo">
                                    <a class = "good__image" href = "{{url('goods/'.$item->id)}}">
                                        <img src = "{{ route('get_photo',[$item->id,'item']) }}" alt = ""/>
                                        <i class = "icon icon--show-preview"></i>
                                    </a>
                                </div>
                                <h4 class = "good__title">{{$item->name}}</h4>
                                <span class = "good__manufacturer">{{get_brand($item->brand,'name')}}</span>
                                {{--<span class = "good__price">1 450,00 ₴</span>--}}
                            </article>
                        </div>
                    @endforeach
                    @if(!count($data['cat_goods']))
                        <h3>Товари відсутні</h3>
                    @endif

                </div>
                {!! $data['paginator']->render()!!}
            </div>
        </div>
    </div>
</section>
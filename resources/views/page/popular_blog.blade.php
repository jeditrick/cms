<section class = "testimonials">
    <div class = "container">
        <div class = "testimonials__slider">
            <div class = "testimonials__slider__slide">
                <div  class = "photo">
                    <img  src = "{{ route('get_photo',[$data['popular_blog']->id,'item']) }}" alt = ""/>
                </div>
                <div class = "text">
                    <h3 class = "title">{{$data['popular_blog']->name}}</h3>
                    <h4 class = "name">{{$data['popular_blog']->author}}</h4>
                    <p>{{$data['popular_blog']->short_description}}</p>
                    <a class = "read" href = "{{url('article/'.$data['popular_blog']->id)}}">Читати детальніше</a>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
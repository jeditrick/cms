<section class = "catalogue-search">
    <div class = "container">
        <div class = "catalogue-search__slider">
            <div class = "catalogue-search__slider__slide">
                <img src = "{{asset('/img/slider.jpg')}}" alt = ""/>
            </div>
            <div class = "catalogue-search__slider__controls">
                <span class = "active"></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class = "catalogue-search__search-block">
            <h1 class = "catalogue-search__search-block__title">Повний каталог українських виробників</h1>

            <form class = "catalogue-search__search-block__form" action = "{{ url('search') }}" method="get">
                <input type = "text" name="search" placeholder = "Назва товару"/>
                <button type = "submit">
                    <i class = "icon icon--search-btn"></i>
                </button>
            </form>
        </div>
    </div>
</section>
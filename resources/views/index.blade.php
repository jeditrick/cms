<!DOCTYPE html>
<!--[if IE 8]>
<html class = "ie8">
<![endif]-->
<!--[if !IE]>-->
<html>
<!--<![endif]-->
<head lang = "en">
    @include('includes.site')
</head>
<body>
@include('page.header')
@include('page.search')
@include('page.categories')
@include('page.goods')
@include('page.popular_blog')
@include('page.popular_brands')
@include('page.footer')
<script src = "{{asset('/js/all.js')}}"></script>
</body>
</html>
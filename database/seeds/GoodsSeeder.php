<?php


use App\Goods;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class GoodsSeeder extends Seeder
{

    public function run()
    {
        $counter = 9;

        Goods::create(
            array(
                'name' => 'Wear item '.$counter,
                'short_description' => 'Nice wear very cool',
                'description' => 'very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description! very long description!',
                'parent_id' => 5,
                'brand' => 2,
                'type' => 1
            )
        );
        Goods::create(
            array(
                'name' => 'Shoes item '.$counter,
                'short_description' => 'short short short short short short short short short short short short short short short short',
                'description' => 'long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ',
                'parent_id' => 3,
                'brand' => 5,
                'type' => 1
            )
        );
        Goods::create(
            array(
                'name' => 'Bags item '.$counter,
                'short_description' => 'short short short short short short short short short short short short short short short short',
                'description' => 'long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ',
                'parent_id' => 4,
                'brand' => 3,
                'type' => 1
            )
        );

        Goods::create(
            array(
                'name' => 'Blog item '.$counter,
                'short_description' => 'short short short short short short short short short short short short short short short short',
                'description' => 'long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ',
                'parent_id' => 25,
                'author' => 'Kolya',
                'type' => 1
            )
        );

        Goods::create(
            array(
                'name' => 'Event item '.$counter,
                'short_description' => 'short short short short short short short short short short short short short short short short',
                'description' => 'long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ',
                'parent_id' => 27,
                'author' => 'Kolya',
                'type' => 1
            )
        );

        Goods::create(
            array(
                'name' => 'News item '.$counter,
                'short_description' => 'short short short short short short short short short short short short short short short short',
                'description' => 'long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long ',
                'parent_id' => 26,
                'author' => 'Kolya',
                'type' => 1
            )
        );

    }


}
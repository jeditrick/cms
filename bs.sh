sudo chmod -R 777 ../uwc7/
sudo apt-get update

sudo apt-get install -y git-core
sudo git config --global user.name "Lopachuk Nikolay"
git config --global user.email "jeditrick@yandex.ru"
sudo git pull origin master

sudo apt-get install -y apache2
sudo apt-get install -y python-software-properties

sudo add-apt-repository ppa:ondrej/php5
sudo apt-get update

sudo apt-get install -y php5
sudo apt-get install -y libapache2-mod-php5 php5-mcrypt php5-mysqlnd

sudo apt-get install -y curl libcurl3 libcurl3-dev php5-curl

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password 12345'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password 12345'
sudo apt-get install -y mysql-server
sudo mysql -u root -h localhost -p12345 -Bse "CREATE DATABASE uwc7"
sudo mysql -uroot -p12345 uwc7 < /var/www/html/uwc7/uwc7.sql

sudo apt-get install -y curl libcurl3 libcurl3-dev php5-curl

#sudo curl -sS https://getcomposer.org/installer | php
#sudo mv /home/vagrant/composer.phar /var/www/html/uwc7/composer.phar
#cd /var/www/html/uwc7/
#sudo php /var/www/html/uwc7/composer.phar install
sudo cp /var/www/html/uwc7/000-default.conf /etc/apache2/sites-available/000-default.conf
sudo a2enmod rewrite

sudo service apache2 restart

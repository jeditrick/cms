<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model {
    protected $table = 'brands';
    protected $fillable = ['name', 'description', 'address', 'picture'];
    public function updateItem($data, $id)
    {
        unset($data['_token']);
        self::find($id)->update($data);
        return (int)$id;
    }
}

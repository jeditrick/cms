<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class Goods extends Model
{

    protected $table = "goods";
    protected $fillable = ['name', 'author' ,'short_description', 'description', 'type', 'picture', 'parent_id', 'brand'];

    public function updateItem($data, $id)
    {
        unset($data['_token']);
        $data['folder_id'] = isset($data['folder_id']) ? $data['folder_id'] : 0;
        self::find($id)->update($data);
        return (int)$id;
    }

}

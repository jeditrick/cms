<?php namespace App\Http\Controllers;

use App\Brands;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ItemRequest;
use Illuminate\Http\Request;

class BrandsController extends Controller {


	public function index()
	{
        $brands = Brands::all();
        return view('admin.brands.brands',compact('brands'));
    }

    public function add(){
        $data = array(
            'headline' => 'Add item'
        );
        $has_photo = false;
        return view('admin.brands.add',compact('data','has_photo'));
    }

    public function edit($id)
    {
        $item = Brands::find($id)->toArray();
        $data = array(
            'headline' => 'Edit item',
            'item' => $item
        );
        $has_photo = ImageController::hasPhoto($id, new Brands());
        return view('admin.brands.add')->with('data', $data)->with('has_photo', $has_photo);
    }

    public function submit(ItemRequest $request)
    {

        $data = $request->all();
        $Brands = new Brands();

        if (isset($data['id'])) {
            $id = $Brands->updateItem($data, $data['id']);
        } else {
            $id = $Brands->create($data)->id;
        }

        if (array_key_exists('image', $data)) {
            ImageController::uploadPhoto($data['image'], $id, new Brands());
        }

        return redirect(url('home/brands'));
    }

    public function delete($id)
    {
        Brands::find($id)->delete();
        return redirect('home/brands/');
    }


}

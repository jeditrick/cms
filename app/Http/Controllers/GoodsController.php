<?php namespace App\Http\Controllers;


use App\Brands;
use App\Goods;
use App\Http\Requests;
use App\Http\Requests\ItemRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class GoodsController extends Controller
{
    private $itemList;
    private $buffList;

    public function __construct()
    {
        $this->itemList = [];
        $this->buffList = [];
        $this->Model = new Goods();
    }

    public function index($folder_id = null)
    {
        $foo = $this->getAllChildren(1, 1)->filter(function ($item) {
            return preg_match('/bag/i', $item->name);
        })->sortBy('created_by', SORT_REGULAR, true);
        if ($folder_id === null) {
            $folder_id = 0;
        }
        $goods = $this->getChildren($folder_id)->toArray();
        if ((int)$folder_id !== 0) {
            $breadcrumbs = new PageController();
            $breadcrumbs = $breadcrumbs->getBreadcrumbs($folder_id);
        } else {
            $breadcrumbs = [];
        }

        return view('admin.goods.goods')->with('goods', $goods)->with('folder_id', $folder_id)->with('breadcrumbs',
            $breadcrumbs);
    }

    public function getGrandParentItem($id)
    {
        return $this->getParentItem($this->getParentItem($id)->toArray()['id']);
    }

    public function getParentItem($id)
    {
        return $this->Model->find((int)$this->Model->find((int)$id)->parent_id);
    }

    public function getNeighbourhoods($id, $type = null)
    {
        switch ($type) {
            case 1:
                return $this->Model->all()->where('parent_id', $this->getParentItem((int)$id)->id)->where('type', 1);
                break;
            case 2:
                return $this->Model->all()->where('parent_id', $this->getParentItem((int)$id)->id)->where('type', 2);
                break;
            default:
                return $this->Model->all()->where('parent_id', $this->getParentItem((int)$id)->id);
                break;
        }
    }

    public function getChildren($id, $type = null)
    {
        switch ($type) {
            case 1:
                return $this->Model->all()->where('parent_id', (int)$id)->where('type', 1);
                break;
            case 2:
                return $this->Model->all()->where('parent_id', (int)$id)->where('type', 2);
                break;
            default:
                return $this->Model->all()->where('parent_id', (int)$id);
                break;
        }
    }

    public function getAllChildren($id, $type, $depp = 0)
    {
        if ($depp++ == 0) {
            $this->buffList = [];
        }
        foreach ($this->getChildren($id) as $el) {
            if ($el->type === $type) {
                $this->buffList[$el->id] = $el;
                if ($type === 2) {
                    $this->getAllChildren($el->id, $type, $depp);
                }
            } else {
                $this->getAllChildren($el->id, $type, $depp);
            }
        }

        return Collection::make($this->buffList);
    }


    public function getTree($id, array $arr = [])
    {
        $arr = [];
        foreach ($this->getChildren($id) as $el) {
            if ($el->type === 2) {
                $arr[$el->id] = $this->getTree($el->id, $arr);
                $this->itemList = $arr;
            } else {
                if ($id !== 0) {
                    $arr[$this->getParentItem($el->id)->id] = $el;
                } else {
                    $arr[$el->id] = $el;
                }
            }
        }

        return $arr;
    }

    public function add($folder_id = null)
    {
        if ((int)$folder_id !== 0) {
            $breadcrumbs = new PageController();
            $breadcrumbs = $breadcrumbs->getBreadcrumbs($folder_id);
        } else {
            $breadcrumbs = [];
        }

        $data = array(
            'headline' => 'Add item',
            'brands' => Brands::all()
        );
        $has_photo = false;

        return view('admin.goods.add_item', compact('data', 'folder_id', 'has_photo', 'breadcrumbs'));
    }

    public function edit($id)
    {
        $breadcrumbs = new PageController();
        $breadcrumbs = $breadcrumbs->getBreadcrumbs($id);
        $item = Goods::find($id)->toArray();
        $data = array(
            'headline' => 'Edit item',
            'item' => $item,
            'brands' => Brands::all()
        );
        $has_photo = ImageController::hasPhoto($id, $this->Model);

        return view('admin.goods.add_item')->with('data', $data)->with('has_photo', $has_photo)->with('breadcrumbs',
            $breadcrumbs);
    }

    public function submit(ItemRequest $request)
    {

        $data = $request->all();

        if (isset($data['id'])) {
            $id = $this->Model->updateItem($data, $data['id']);
            $redirect = redirect(url('home/item/' . $data['id'] . '/edit'));
        } else {
            $id = $this->Model->create($data)->id;
            $redirect = redirect(url('home/item/' . $data['parent_id'] . '/open'));
        }

        if (array_key_exists('image', $data)) {
            ImageController::uploadPhoto($data['image'], $id, $this->Model);
        }

        return $redirect;
    }

    public function delete($id)
    {
        $parent = $this->Model->find($id)->parent_id;
        $this->Model->find($id)->delete();

        return redirect('home/item/' . $parent . '/open');
    }

    public function back($id)
    {
        $parent = $this->getParentItem($id);

        return redirect('home/' . $parent);
    }
}

<?php namespace App\Http\Controllers;

use App\Brands;
use App\Goods;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class PageController extends Controller
{
    private $breadcrumbs;
    private $GoodsController;
    private $sharedData;

    public function __construct()
    {
        $this->GoodsController = new GoodsController();
        $this->breadcrumbs = [];
        $this->sharedData = [
            'brands' => Brands::all(),
            'popular_brands' => Brands::all()->take(4),
            'goods_tree' => $this->GoodsController->getTree(1),
            'articles_tree' => $this->GoodsController->getTree(24),
            'popular_blog' => Goods::all()->where('parent_id', 25)->sortBy('created_at', SORT_REGULAR,
                true)->take(1)[0],
            'types' => $this->GoodsController->getAllChildren(1, 2),
            'footer_info' => [
                '29' => $this->GoodsController->getAllChildren(29,1),
                '69' => $this->GoodsController->getAllChildren(69,1),
                '70' => $this->GoodsController->getAllChildren(70,1),
                '26' => $this->GoodsController->getAllChildren(26,1)->sortBy('created_at', SORT_REGULAR,
                    true)->take(5)
            ]
        ];
    }

    public function index($id = null)
    {
        $id = ($id === null || $id === 0) ? 1 : $id;

        $search = Request::all();

        if (isset($search['search'])) {
            $searchword = $search['search'];
            $items = $this->GoodsController->getAllChildren(1, 1)->filter(function ($item) use ($searchword) {
                return preg_match('/' . $searchword . '/i', $item->name);
            });
            $url = 'search';
            $search = true;
        } else {
            $search = false;

            $items = $this->GoodsController->getAllChildren($id, 1);
            $url = 'items/cat/' . $id;
        }

        $size = count($items);
        $per_page = 6;

        $pagination = new LengthAwarePaginator($items, $size, $per_page);
        $pagination->setPath(url($url));
        if ($search) {
            $pagination->appends(['search' => $searchword]);
        }
        $pagination->getCollection()->make();
        $paginated_items = array_chunk($pagination->items(), $per_page);


        $data = [
            'id' => $id,
            'brand_id' => 0,
            'cat_goods' => count($paginated_items) ? $paginated_items[$pagination->currentPage() - 1] : [],
            'paginator' => $pagination,
            'cat_active' => Goods::find($id),
            'breadcrumbs' => $this->getBreadcrumbs($id)

        ];

        $i = 0;

        return view('index')->with('data', array_merge($this->sharedData, $data))->with('i', $i);

    }

    public function blog()
    {
        return $this->renderArticles(25);
    }

    public function events()
    {
        return $this->renderArticles(27);
    }

    public function news()
    {
        return $this->renderArticles(26);
    }

    public function renderArticles($id)
    {
        $data = [
            'id' => $id,
            'articles' => $this->GoodsController->getAllChildren($id, 1)->sortBy('created_at', SORT_REGULAR,
                true)->take(8)->toArray()
        ];

        return view('blog')->with('data', array_merge($this->sharedData,$data));
    }


    public function brandSearch($id)
    {
        $GoodsController = new GoodsController();
        $id = ($id === null) ? 1 : $id;

        $size = count(Goods::all()->where('brand', (int)$id)->where('type', 1));
        $per_page = 6;

        $pagination = new LengthAwarePaginator(Goods::all()->where('brand', (int)$id)->where('type', 1), $size,
            $per_page);
        $pagination->setPath(url('brands/' . $id . '/search'))->getCollection()->make();
        $paginated_items = array_chunk($pagination->items(), $per_page);

        $data = [
            'brand_id' => $id,
            'id' => 0,
            'brands' => Brands::all(),
            'cat_goods' => count($paginated_items) ? $paginated_items[$pagination->currentPage() - 1] : [],
            'popular_blog' => Goods::all()->where('parent_id', 25)->sortBy('created_by')->take(1)[0],
            'paginator' => $pagination,
            'cat_active' => Goods::find(1),
            'breadcrumbs' => $this->getBreadcrumbs(1)

        ];
        $i = 0;

        return view('index')->with('data', array_merge($this->sharedData, $data))->with('i', $i);
    }

    public function article($id)
    {
        $GoodsController = new GoodsController();
        $data = [
            'id' => $id,
            'article' => Goods::find($id),
            'parent' => $GoodsController->getParentItem($id)
        ];

        return view('article')->with('data', array_merge($this->sharedData,$data));
    }

    public function getBreadcrumbs($id)
    {
        if ($id !== 0) {
            $parent_instance = Goods::find($id);
            $parent_id = $parent_instance->parent_id;
            $this->breadcrumbs[$parent_id] = $parent_instance;
        } else {
            return $this->breadcrumbs;
        }

        do {
            if ($parent_id === 0) {
                return $this->breadcrumbs;
            }
            $parent_instance = Goods::find($parent_id);
            $parent_id = $parent_instance->parent_id;
            $this->breadcrumbs[$parent_id] = $parent_instance;
        } while ($parent_id !== 0);

        return array_reverse($this->breadcrumbs);
    }

    public function goods($id)
    {
        $id = ($id === null || $id === 0) ? 1 : $id;
        $parent_id = Goods::find($id)->parent_id;

        $search = Request::all();
        $GoodsController = new GoodsController();

        if (isset($search['search'])) {
            $searchword = $search['search'];
            $items = $GoodsController->getAllChildren(1, 1)->filter(function ($item) use ($searchword) {
                return preg_match('/' . $searchword . '/i', $item->name);
            });
            $url = 'search?search=' . $searchword;

        } else {
            $items = $GoodsController->getAllChildren(Goods::find($id)->parent_id, 1);
            $url = 'items/cat/' . $id;
        }

        $size = count($items);
        $per_page = 6;

        $pagination = new LengthAwarePaginator($items, $size, $per_page);
        $pagination->setPath(url('goods/' . $id))->getCollection()->make();
        $paginated_items = array_chunk($pagination->items(), $per_page);


        $alike = $this->GoodsController->getAllChildren(1, 1)->filter(function ($item) use ($id) {
            return $item->brand = Goods::find($id)->brand;
        })->take(6);

        $data = [
            'id' => $parent_id,
            'brand_id' => Goods::find($id)->brand,
            'brands' => Brands::all(),
            'cat_goods' => count($paginated_items) ? $paginated_items[$pagination->currentPage() - 1] : [],
            'paginator' => $pagination,
            'popular_blog' => Goods::all()->where('parent_id', 3)->sortBy('created_by')->take(1)[0],
            'cat_active' => Goods::find($id),
            'breadcrumbs' => $this->getBreadcrumbs($id),
            'goods' => Goods::find($id),
            'alike' => $alike
        ];

        $i = 0;

        return view('goods')->with('data', array_merge($this->sharedData, $data))->with('i', $i);
    }
}

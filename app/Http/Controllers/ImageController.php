<?php namespace App\Http\Controllers;

use App\Brands;
use App\Goods;
use App\Http\Controllers\Controller;

use Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageController extends Controller
{


    public static function uploadPhoto(UploadedFile $file, $id, $model)
    {
        $time = date('Y_m_d');
        $name = $time . '_' . $file->getClientOriginalName();
        Storage::disk('local')->put(
            $name,
            File::get($file)
        );

        return $model::find($id)->update(['picture' => ($name)]);
    }


    public function getPhoto($id, $model)
    {
        if ($model === 'brands') {
            $model = new Brands();
        } else {
            $model = new Goods();
        }
        $item = $model::find($id)->toArray();
        $file = Storage::disk('local')->get($item['picture']);
        if ($file === '') {
            $file = Storage::disk('local')->get('image-not-found.gif');
        }

        return (new Response($file, 200))->header('Content-Type', 'image/jpeg');
    }

    public static function hasPhoto($id, $model)
    {
        return ($model::find($id)->picture !== '');
    }

    public function deletePhoto($id, $model)
    {
        if ($model === 'brands') {
            $model = new Brands();
            $redirect = redirect(url('home/brand/' . $id . '/edit'));
        } else {
            $model = new Goods();
            $redirect = redirect(url('home/item/' . $id . '/edit'));

        }
        Storage::disk('local')->delete($model::find($id)->picture);
        $model::find($id)->update(['picture' => '']);

        return $redirect;
    }


}

<?php
function has_children($var)
{
    return count($var);
}

function get_obj($id)
{
    $Goods = new \App\Goods();
    return $Goods::find($id);
}

function get_brand($id, $item = null)
{
    if ($id !== 0 && $id !== null) {
        $Brands = new \App\Brands();
        if ($item !== null) {
            return $Brands::find($id)->$item;
        }else{
            return $Brands::find($id);
        }
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//================site============
Route::get('/', 'PageController@index');
Route::get('search', 'PageController@index');
Route::get('items/cat/{id}', 'PageController@index');
Route::get('news', 'PageController@news');
Route::get('news/{id}', 'PageController@news');
Route::get('blog', 'PageController@blog');
Route::get('blog/{id}', 'PageController@blog');
Route::get('events', 'PageController@events');
Route::get('events/{id}', 'PageController@events');
Route::get('about', 'PageController@about');

Route::get('goods/item/{id}', 'PageController@about');


Route::get('brands/{id}/search', 'PageController@brandSearch');

Route::get('article/{id}', 'PageController@article');

Route::get('goods/{id}', 'PageController@goods');


//================admin============
Route::get('home', 'HomeController@index');
Route::get('home/goods', 'GoodsController@index');
Route::get('home/item/{folder_id}/open', 'GoodsController@index');
Route::get('home/item/{folder_id}/add', 'GoodsController@add');
Route::get('home/item/{id}/edit', 'GoodsController@edit');
Route::get('home/item/{id}/delete', 'GoodsController@delete');
Route::post('home/submit_item', 'GoodsController@submit');

Route::get('photo/{id}/{model}/get',  [
    'as' => 'get_photo', 'uses' => 'ImageController@getPhoto'] );
Route::get('photo/{id}/{model}/delete',  [
    'as' => 'delete_photo', 'uses' => 'ImageController@deletePhoto'] );

Route::get('home/brands', 'BrandsController@index');
Route::get('home/brand/add', 'BrandsController@add');
Route::post('home/brand/submit', 'BrandsController@submit');
Route::get('home/brand/{id}/edit', 'BrandsController@edit');
Route::get('home/brand/{id}/delete', 'BrandsController@delete');




//Route::get('home/back', 'GoodsController@back');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

